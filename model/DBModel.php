<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    protected $db = null;
    public function __construct($db = null)
    {
        $dsn = "mysql:host=localhost;dbname=test;charset=utf8mb4";
        $user = "cato";
        $pass = "IMT2571::cato";

        if ($db)
        {
            $this->db = $db;
        }
        else{
          try {
                 $this->db = new PDO($dsn, $user, $pass);
                 $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                 $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             } catch (PDOException $e) {
                 echo ("Something went terribly wrong, please consult an adult - Connection to DB has failed");
                 $e->getMessage();
    }
  }
}
    public function getBookList(){
        try {
          $array = [];
          $stmt = $this->db->query("SELECT * FROM Book");
            foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $cnt) {
              $array[] = new Book($cnt['title'], $cnt['author'], $cnt['description'], $cnt['id']);
            }
            return $array;
        }
        catch (PDOException $e) {
         echo ("Something went terribly wrong, please consult an adult - Booklist unavailable");
          $e->getMessage();
      }
            return null;
    }

    public function getBookById($id)
    {
        try {
        $stmt = $this->db->prepare("SELECT * FROM Book WHERE id=:id");
        $stmt->bindValue("id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $book = array();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row) {
            $book = new Book($row["title"], $row["author"], $row["description"], $id);
            return $book;
        }
    }   catch(PDOException $e){
      echo ("Something went terribly wrong, please consult an adult");
            $e->getMessage();
  }
}

    public function addBook($book)
    {
        try {
        $stmt = $this->db->prepare(
            "INSERT into Book (title, author, description) VALUES (:title, :author, :description)");

        $stmt->bindValue("title", $book->title);
        $stmt->bindValue("author", $book->author);
        $stmt->bindValue("description", $book->description);
            if(!empty($book->author) && ($book->title)){
              $stmt->execute();
            }
        $book->id = $this->db->lastInsertId();
    }
        catch(PDOException $e){
          echo ("Something went terribly wrong, please consult an adult");
       
          $e->getMessage();
  }
}

    public function modifyBook($book)
    {
        try {
        $stmt = $this->db->prepare(
            "UPDATE Book SET title = :title, author = :author, description = :description WHERE id = :id");
        $stmt->bindValue("id", $book->id);
        $stmt->bindValue("title", $book->title);
        $stmt->bindValue("author", $book->author);
        $stmt->bindValue("description", $book->description);
        if(!empty($book->author) && ($book->title)){
          $stmt->execute();
        }
    }  catch(PDOException $e){
        echo ("Something went terribly wrong, please consult an adult");
          $e->getMessage();
  }
}

    public function deleteBook($id)
    {
        $stmt = $this->db->prepare("DELETE FROM Book WHERE id = :id");
        $stmt->bindValue("id", $id, PDO::PARAM_INT);
        $stmt->execute();
  }
}

?>
